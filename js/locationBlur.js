var app = app || {};

(function() {

        var appListElementSelector = '.loc-blur-app-list',
	    appMgr = typeof navigator.mozApps !== 'undefined' ? navigator.mozApps.mgmt : undefined,
	    hidden_roles =  ['system', 'keyboard', 'homescreen', 'input', 'search' ]
	    ;	
        app.insertAppList = function(callback) 
        {
          if(!appMgr) {
            console.log('App manager not found. Are you using Firefox OS?');
            if(typeof callback === 'function') {
                callback();
            }
            return;
          }
          var listElements = document.querySelectorAll(appListElementSelector);
          var listHtml = '';
          appMgr.getAll().onsuccess = function onsuccess(event) 
          {
            var apps = event.target.result;
            apps.sort(function(a,b){
                return (a.manifest.name < b.manifest.name)? -1 : 1; 
                });
            var i = 0;
            apps.forEach(function eachApp(app) {
                if (hidden_roles.indexOf(app.manifest.role) == -1
                    && app.manifest.name != 'Privacy Panel' && app.manifest.name != 'Guest Mode') {
                    listHtml +=
                    '<li>' +
                    '<button class="icon icon-view" id="' + app.manifest.name + '" >' + app.manifest.name + '</button>' +
                    '</li>'
                    ;
                    i++;
                 }
            });
            if(listElements) {
                for(var index in listElements) {
                    var listElement = listElements.item(index);
                    listElement.innerHTML = listHtml;
                }
            }
            if(typeof callback === 'function') {
                callback();
            }
          };
        };
        window.onload=function() {
        console.log(document.getElementById('AAAA'));
        document.getElementById('AAAA').onclick =
    	function deleteCertificate() {
      	console.log('openWindow was called');
        }
        };
})();